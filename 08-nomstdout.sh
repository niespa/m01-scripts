#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripció: Fer un programa que rep com arguments nom d'usuaris
# --------------------------------
ERR_NARGS=1
# 1) Validar arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
 fi

# 2) xixa

for user in $*
do
  grep "^$user:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ]; then
    echo $user
  else
    echo $user >> /dev/stderr
  fi
done

