#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripció: Fer un comptador des de zero fins al valor indicat per l'argument rebut
# --------------------------------
ERR_NARGS=1
# 1) Validar arguments
if  [ $# -ne 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
 fi

 #2) validar numero i xixa
num=0
MAX=$1
while [ $num -le $MAX ]
do
	echo $num
	((num++))
done

exit 0
