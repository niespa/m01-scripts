#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripció: Fer un programa que rep com a arguments números de més (un o més) i indica per a cada mes rebut quants dies té el més.
# --------------------------------
#1)Validar d'argumentsmes
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
exit $ERR_NARGS
fi

#2) validar
mes=$1
for mes in $*	  
do
 case $mes in
 2) echo "28";;

 1|3|5|7|8|10|12) echo "31";;

 4|6|9|11) echo "30";;
 
 *) echo "No es un mès vàlid" >> /dev/stderr
esac
done
exit 0
