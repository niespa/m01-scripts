#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripció:Processar stdin cmostrant per stdout les línies numerades i en majúscules..
# ------------------------------
num=1
#1) xixa
while read -r line
do
	echo "$num:$line" | tr 'a-z' 'A-Z'
		((num++))
done
exit 0

