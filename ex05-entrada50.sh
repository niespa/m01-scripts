#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripció: Moatrar línia a línia l'emtrada estàndard, retallant només els primers 50 caràcters
# -------------------------------

# 1) xixa
while read -r line
do	
  echo $line | cut -c1-50
done


