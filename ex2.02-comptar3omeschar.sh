#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripció: Processar els arguments i comptar quantes n’hi ha de 3 o més caràcters.
# ------------------------------
num=0
#1)xixa

for arg in $*
do
	echo $arg | grep -E -q "^.{3,}"
	if [ $? -eq 0 ] ; then
	((num++))
	fi
done
echo $num
exit 0


