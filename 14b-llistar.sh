#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# a)rep un arg i és un directori i es llista
# b) llistar numerant els arguments del dir
# -------------------------------
ERR_NARGS=1
ERR_NODIR=2
# 1) validar que hi ha un arg
if [ $# -ne 1 ]
then
 echo "Error: num arguments incorrecte"
 echo "Usage: $0 arg "
 exit $ERR_NARGS
fi
dir=$1

# 2) validar que és un dir
if ! [ -d $dir ]; then
  echo "ERROR: $dir no és un directori"
  echo "usage: $0 dir"
  exit $ERR_NODIR
fi

# 3) xixa: llistar
num=1
llista=$(ls)
for elem in $llista
do
	echo "$num:$elem"
	((num++))
done
exit 0
