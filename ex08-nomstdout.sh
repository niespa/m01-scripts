#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripció: Fer un programa que rep com arguments nom d'usuaris
# --------------------------------

# 1)Validacio i xixa

for user in $*
do
  grep "$user:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ]; then
    echo $user esta en el sistema
  else
    echo $user no esta en el sistema >> /dev/stderr
  fi
done

