#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripció: exemples bucle for
# --------------------------------
#8)llistar els logins ordenats i numerats
# cut -d: -f1 /etc/passwd | sort
llistat=$(cut -d: -f1 /etc/passwd | sort)
num=1
for elem in $llistat
do
  echo "$num:$elem"
  num=$((num+1))
done
exit 0

#7) lliatar numerant les lineas
llistat=$(ls)
num=1
for elem in $llistat
do
  echo "$num:$elem"
  num=$((num+1))
done
exit 0

#6)Itarar per cada un dels valors que generei ls
llistat=$(ls)
for elem in $llistat
do
  echo "$elem"
 done
exit 0

#5)Numerar els arguments
num=1
for arg in $*
do
 echo "$num:$arg"
 num=$((num+1))
done
exit 0

#4) $@ expandeix $* no
for arg in "$@"
	do
	  echo "$arg"
done
exit 0

#3) iterar per llista d'arguments
for arg in "$*"
	do
	  echo "$arg"
done
exit 0
#2) iterar noms
for nom in "pere marta anna pau"
do
  echo "$nom"
done
exit 0
# 1) iterar noms
for nom in "pere" "marta" "anna" "pau"
do
  echo "$nom"
done
exit 0
