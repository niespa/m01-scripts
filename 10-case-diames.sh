#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripcio: dir els dies que té un més
# Synopsis: prog mes
#    a) validar rep un arg
#    b) validar mes [1-12]
#    c) xixa
# --------------------------------
ERR_NARGS=1
ERR_MES=2
#	a) rep un argument
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 mes"
  exit $ERR_NARGS	
 fi
# b)validar mes
if ! [ $1 -ge 1 -a $1 -le 12 ]
then
 echo "Error: mes $mes no vàlid"
 echo "Usage: $0 mes"
 exit $ERR_MES
fi 
# c)xixa	
case $1 in
	"1"|"3"|"5"|"7"|"8"|"10"|"12")
   	echo "$1 té 31 dies";;
        "2")
	echo "$1 te 28 dies";;
	*)
	echo "$1 te 30 dies";;
esac

