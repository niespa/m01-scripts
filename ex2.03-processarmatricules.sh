#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripció:Processar arguments que són matricules:
#a) Llistar les vàlides, del tipus: 9999-AAA.
#b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status el
#número d’errors (de no vàlides).
# ------------------------------
ERR_NARGS=1
# 1) Validar arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
 fi

err=0
# 2) xixa

for matricula in $*
do
  echo $matricula | grep -E -q "^[0-9]{4}-[A-Z]{3}$"
  if [ $? -eq 0 ] ; then
     echo $matricula
  else 
	  ((err++))
	  echo  "Error: matricula no valida" >> /dev/stderr
  fi
  
	
  

done
exit $err
