#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripció: Moatrar els arguments rebuts línia a línia, tot numerànt-los
# --------------------------------
ERR_NARGS=1
# 1) Validar arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
 fi

# 2) xixa
num=1
for arg in $*
do
 echo "$num:$arg"
 num=$((num+1))
done
exit 0
