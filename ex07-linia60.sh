#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripció: Procesar línia a línia l'entrada estàndard, si la línia té mes de 60 caràcters la mostra, si no no
# --------------------------------

# 1) xixa
while read -r line
do	
  char=$(echo "$line" | wc -c)
  if [ "$char" -gt 60 ]; then
    echo $line
  fi	  
done


