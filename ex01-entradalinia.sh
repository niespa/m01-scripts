#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripció: Moatrar l'entrada estàndard numerant línia a línia
# --------------------------------
ERR_NARGS=1
# 1) Validar arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
 fi

# 2) Xixa
num=1
while read -r line
do
	echo "$num:$line"
	((num++))
done
exit 0
