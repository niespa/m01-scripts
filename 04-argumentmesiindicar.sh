#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripció: Fer un programa que rep com a arguments números de més (un o més) i indica per a cada mes rebut quants dies té el més.
# --------------------------------
ERR_NARGS=1
# 1) Validar arguments
if  [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
 fi

# b)validar mes
if ! [ $1 -ge 1 -a $1 -le 12 ]
then
 echo "Error: mes $mes no vàlid"
 echo "Usage: $0 mes"
 exit $ERR_MES
fi 
# c)xixa	
case $1 in
	"1"|"3"|"5"|"7"|"8"|"10"|"12")
   	echo "$1 té 31 dies";;
        "2")
	echo "$1 te 28 dies";;
	*)
	echo "$1 te 30 dies";;
esac
