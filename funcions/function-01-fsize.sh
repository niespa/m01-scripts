#! /bin/bash
#exemple de funcions amb disc
#@edt abril 2024
#-------------------------------------
#Exercici 1
function fsize(){
 login=$1
 line=$(grep "^$login:" /etc/passwd)
 if [ -z "$line" ]; then
	echo "error:...."
	return 1
 fi
 dirHOME=$(echo $line | cut -d: -f6)
 du -sh $dirHOME
}


#Exercici 2 rep arguments que són logins i calcula el fsize del home
function loginargs (){
 if [ $# -eq 0 ]; then
	echo "error...."
	return 1
 fi
 for login in $*
 do
   fsize $login
 done
}

#Exercici 3 rep un argument que és un fitxer cada línia del fitxer té un login
function loginfile(){
 if [ ! -f $1 ]; then
	echo "error..."
	return 1
 fi
fileIn=$1

 while read -r login
 do
	 fsize $login
 done < $fileIn
 while read -r login
 do
	 fsize $login
 done < $fileIn
}

#Exercici inventat Eduard

function loginstdin (){
 while read -r login
 do
	 fsize $login
 done
}

#Excercici 4 processa el fitxer rebut o stdin si no en rep cap
function loginboth (){
fileIn="/dev/stdin"
 if [ $# -eq 1 ]; then
	fileIn=$1
 fi

 while read -r login
  do
	 fsize $login
  done < $fileIn
}


