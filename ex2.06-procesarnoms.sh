#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Processar per stdin linies d’entrada tipus “Tom Snyder” i mostrar per stdout la línia en format → T. Snyder.
# --------------------------------
while read -r line
do
	nom=$(echo $line | cut -c1)
	cognom=$(echo $line | cut -d " " -f2)
	echo "$nom. $cognom"
done
