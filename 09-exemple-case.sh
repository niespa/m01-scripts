#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Exemple case
# --------------------------------
#2) dl dt dc dj dc ----> laborables
#ds dm -> festiu
case $1 in
  "dl"|"dt"|"dc"|"dj"|"dv")
      echo "$1 es un dia lavorable";;
  "ds"|"dg")
      echo "$1 és un dia festiu";;
  *)
      echo "això ($1) no és un dia";;
esac
exit 0

#1) exemple vocals
case $1 in
  [aeiou])
    echo "$1 és una vocal"
    ;;
  [bcdfghijklmnñopqrstvwxyz])
    echo "$1 és una consonant"
    ;;
   *)
     echo "$1 és una altra cosa"
     ;;
esac
exit 0
