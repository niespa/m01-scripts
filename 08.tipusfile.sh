#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# indicar si dir és: regular, dir, link
# --------------------------------
ERR_NARGS=1
ERR_NOTA=2
#	1) rep un argument
if [ ! -e $fit ]
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 fit"
  exit $ERR_NARGS
 fi
 # 2) xixa
if [ ! -e $fit ]; then 
  echo "$fit no existeix"
  exit $ERR_NOEXIST
elif [ -f $fit ]; then
  echo "$fit és un regular file"
elif [ -h $fit ]; then
  echo "$fit es un link"
elif [ -d $fit ]; then
  echo "$fit es un directori
else "es un altre cosa"
fi

