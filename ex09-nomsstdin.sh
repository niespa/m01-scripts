#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripció: Moatrar usuaris per stdin
# --------------------------------

# 1) xixa
while read -r line
do	
  grep "^$line:" /etc/passwd &> /dev/stdout
  if [ $? -eq 0 ]; then
    echo $line usuari existent
  else
    echo $line nom de sistema inexistent>> /dev/stderr
  fi	  
done

