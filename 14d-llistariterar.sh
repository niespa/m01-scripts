#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# a)rep un arg i és un directori i es llista
# b) llistar numerant els arguments del dir
# c) per cada element dir si es dir, regular, o altra cosa
# -------------------------------
ERR_NARGS=1
ERR_NODIR=2
# 1) validar que hi ha un arg
if [ $# -eq 0 ]
then
 echo "Error: num arguments incorrecte"
 echo "Usage: $0 arg "
 exit $ERR_NARGS
fi
dir=$1

# 2) Iterar per cada argument
for dir in $*
do
  if [ ! -d $dir ];then
     echo "ERROR: $dir Nodirectori" >> /dev/stderr
  else 
#3)xixa llistar
  llista=$(ls $dir)
  echo "Llistat: $dir ----------------"
  for elem in $llista
  do
    if [ -f $dir/$elem ] ; then
	  echo "$elem és un regular file"
    elif [ -d $dir/$elem ] ; then
	  echo "$elem és un directori"
    else
	  echo "es un altre cosa"
   fi
 done
 fi

