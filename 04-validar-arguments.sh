#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Validar que té exàctament 2 args
# i mostrar-los
#
# 04-validar-arguments nom cognom
# ----------------------------------
# 1) valida que hi ha 2 args
if [ $# -ne 2 ]
then
 echo "Error: num arguments incorrecte"
 echo "Usage: $0 nom cognom"
 exit 1
fi
# 2) xixa que els mostra
echo "nom: $1"
echo "cognom: $2"
exit 0
