#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripció: Fer un programa que rep com a arguments noms de dies de la setmana i mostra quants dies eran laborables i quants festius. Si l'argument no és un dia de la setmana genera un error per stderr.
# --------------------------------
ERR_NARGS=1
# 1) Validar arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
 fi

# 2) xixa
laborable=0
festiu=0
for dia in $*
do
  case $dia in
  dilluns|dimarts|dimecres|dijous|divendres|Dilluns|Dimarts|Dimecres|Dijous|Divendres) 
    laborables=$((laborables+1));;
  dissabte|diumenge|Dissabte|Diumenge) 
    festius=$((festius+1));;
  *) 
    echo "dia: $dia no vàlid" >> /dev/stderr;; 
  esac
done
echo "dies laborables: $laborables"
echo "dies festius: $festius"
exit 0

