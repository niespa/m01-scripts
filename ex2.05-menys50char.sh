#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripció: Processar stdin mostrant per stdout les línies de menys de 50 caràcters
# ------------------------------
while read -r line
do
	echo $line | grep -E "^.{,50}$"
done
exit 0
