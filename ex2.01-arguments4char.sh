#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripció: Processar els arguments i mostrar per stdout només el de 4 o més caràcters
# ------------------------------

# 1) xixa
 
for arg in $*
do
  echo $arg | grep -E "^.{4,}"
done

exit 0

