#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
#
# llistar el directori rebut
# 	a) verificar rep un arg
# 	b) verificar que és un directori
# -----------------------------------------------
ERR_ARGS=1
ERR_NODIR=2

#	a) rep un argument
if [ $# -ne 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_ARGS
 fi
# b) verificar que és un directori#
if [ ! -d $1 ] 
then
  echo "ERROR: $1 no és un directori"
  echo "usage: $0 dir"
  exit $ERR_NODIR
fi
# 3) xixa

ls $1
exit 0
