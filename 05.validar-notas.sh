#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Validar nota: suspès, aprovat
# 	a) rep un argument
# 	# 	b) és del 0 al 10
 
# --------------------------------
ERR_NARGS=1
ERR_NOTA=2
#	a) rep un argument
if [ $# -ne 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 nota"
  exit 1
 fi
 # 	b) és del 0 al 10
if ! [ $1 -ge 0 -a $1 -le 10 ]
then
  echo "Error nota $1 no valida"
  echo "Nota: pren valors del 0 al 10"
  echo "Usage: $0 nota"
  exit $ERR_NOTA
fi

# 3) xixa
nota=$1
if [ $nota -lt 5 ]
then
  echo "Nota $nota: suspès"
 else
  echo "Nota $nota: aprobat"
  exit 0
fi
