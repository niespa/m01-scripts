#! /bin/bash
# @ nil ASIX-M01 CUrs 2023- 2024
# Febrer 2024
# Descripció: Fer un programa que rep com a argument un número indicatiu del número màxim
# --------------------------------
ERR_NARGS=1
# 1) Validar arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args"
  exit $ERR_NARGS
 fi

# 2) xixa
num=1
MAX=$1
while read -r line
do	
  if [ "$num" -le $MAX ]; then
    echo "$num: $line"
  else
    echo "$line"      	  
  fi	  
  num=$((num+1))
done


