#! /bin/bah
ERR_NARGS=1
# 1) Validar arguments 
if [ $# -eq 0 ] 
then  
       	echo "Error: num arguments incorrecte"   
	echo "Usage: $0 args"   
	exit $ERR_NARGS  
fi

# 2) xixa
fileIn=$1
err=0
while read -r line
do
  echo $line | grep -E "^[A-Z]{4}[0-9]{3}$" 
   if [ $? -ne 0 ]; then    
	   echo $line  >> /dev/stderr
		status=3   
	   fi 
   done < $fileIn
exit $status

